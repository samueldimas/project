from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'home.html')

def about(request):
    return render(request, 'about.html')

def track_record(request):
    return render(request, 'track_record.html')

def contact(request):
    return render(request, 'contact.html')

def register_now(request):
    return render(request, 'register_now.html')

