from django.urls import path
from . import views

app_name = 'lab_04'

urlpatterns = [
    path('', views.home, name='home'),
    path('about', views.about, name='about'),
    path('track-record', views.track_record, name='track-record'),
    path('contact', views.contact, name='contact'),
    path('register-now', views.register_now, name='register-now'),
]
