from django import forms
from .models import ActivityCategory

class ActivityForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Activity Name',
        'required': True,
    }))
    venue = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Venue',
        'required': True,
    }))
    time = forms.DateTimeField(widget=forms.DateTimeInput(format='%d/%m/%Y %H:%M:%S', attrs={
        'class': 'form-control',
        'placeholder': 'MM/dd/YY HH:mm:ss',
        'required': True,
    }))
    category = forms.ChoiceField(choices=ActivityCategory.CHOICES, widget=forms.Select(attrs={
        'class': 'form-control',
        'placeholder': 'Category',
        'required': True,
    }))
