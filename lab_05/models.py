from django.db import models
import calendar

# Create your models here.
class ActivityCategory(models.Model):
    CATEGORY_CLASS = 'Class'
    CATEGORY_QUIZ = 'Quiz'
    CATEGORY_LAB = 'Lab'
    CATEGORY_TEST = 'Test'
    CATEGORY_MEETING = 'Meeting'
    CATEGORY_PLAY = 'Play'
    CHOICES = (
        (CATEGORY_CLASS, 'Class'),
        (CATEGORY_QUIZ, 'Quiz'),
        (CATEGORY_LAB, 'Lab'),
        (CATEGORY_LAB, 'Test'),
        (CATEGORY_LAB, 'Meeting'),
        (CATEGORY_LAB, 'Play'),
    )
    name = models.CharField(max_length=200, choices=CHOICES, default=CATEGORY_CLASS)

class Activity(models.Model):
    name = models.CharField(max_length=200)
    venue = models.CharField(max_length=200)
    time = models.DateTimeField()
    category = models.ForeignKey(ActivityCategory, on_delete=models.CASCADE)

    def day(self):
        return calendar.day_name[self.time.weekday()]
