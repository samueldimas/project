from django.urls import path
from . import views

app_name = 'lab_05'

urlpatterns = [
    path('', views.index, name='list'),
    path('new', views.createActivityPage, name='createJadwal'),
    path('delete', views.delete_all, name='deleteJadwal'),
]
