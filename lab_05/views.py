from django.shortcuts import render, redirect
from .models import Activity, ActivityCategory
from .forms import ActivityForm

def index(request):
    activities = Activity.objects.all().order_by('-time')
    return render(request, 'jadwal.html', {'activities': activities})

def createActivityPage(request):
    if request.method == 'POST':
        form = ActivityForm(request.POST)
        if form.is_valid():
            activity = Activity()
            activity.name = form.cleaned_data['name']
            activity.venue = form.cleaned_data['venue']
            activity.time = form.cleaned_data['time']
            category = ActivityCategory(name=form.cleaned_data['category'])
            category.save()
            activity.category = category
            activity.save()

        return redirect('/list')
    else:
        form = ActivityForm()
        return render(request, 'create.html', {'form': form})

def delete_all(request):
    Activity.objects.all().delete()
    ActivityCategory.objects.all().delete()
    return redirect('/list')
