from django.contrib import admin
from .models import Activity, ActivityCategory

admin.site.register(Activity)
admin.site.register(ActivityCategory)
