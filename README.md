# Lab PPW 2018 Gasal Samuel Dimas Partogi

Samuel's PPW Lab Projects 2018 

## URL

These lab projects can be accessed from [https://ppw-b-samueldimas.herokuapp.com](https://ppw-b-samueldimas.herokuapp.com)

## Contents

* [Lab_04](https://ppw-b-samueldimas.herokuapp.com/lab_04)
* [Lab_05](https://ppw-b-samueldimas.herokuapp.com/list)  

## Authors

* **Samuel Dimas Partogi** - [samueldimas](https://gitlab.com/samueldimas)

## Acknowledgments

* PPW 2018 Gasal
* Lecturer : Bu Maya Retno
